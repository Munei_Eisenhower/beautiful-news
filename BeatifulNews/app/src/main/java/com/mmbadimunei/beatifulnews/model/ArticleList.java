package com.mmbadimunei.beatifulnews.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ArticleList {

    @SerializedName("items")
    @Expose
    private ArrayList<Article> articles = new ArrayList<>();

    public ArrayList<Article> getArticles() {
        return articles;
    }


    public void setArticles(ArrayList<Article> articles) {
        this.articles = articles;
    }


    @SerializedName("Entities")
    @Expose
    private ArrayList<ArticleSearch> articlesSearch = new ArrayList<>();

    public ArrayList<ArticleSearch> getArticlesSearch() {
        return articlesSearch;
    }


    public void setArticlesSearch(ArrayList<ArticleSearch> articlesSearch) {
        this.articlesSearch = articlesSearch;
    }
}