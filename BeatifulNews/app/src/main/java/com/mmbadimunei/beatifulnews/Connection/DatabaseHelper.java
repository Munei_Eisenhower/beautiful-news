package com.mmbadimunei.beatifulnews.Connection;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by eisenhower on 5/11/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {


    private static final int databaseVersion = 1;
    private static final String databaseName = "db_beautiful";

    private SQLiteDatabase mDB;

    public DatabaseHelper(Context context) {
        super(context, databaseName, null, databaseVersion);

        this.mDB = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        BeautifulTable.onCreate(sqLiteDatabase);


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        BeautifulTable.onUpgrade(sqLiteDatabase, i, i1);
    }


    public Cursor getAllArticles() {
        String Query = "Select * from " + BeautifulTable.TABLE_ARTICLE;
        Cursor cursor = mDB.rawQuery(Query, null);

        return cursor;

    }

    public int getArticleCount() {

        int count = 0;
        String countQuery = "SELECT  * FROM " + BeautifulTable.TABLE_ARTICLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        count = cursor.getCount();
        cursor.close();
        return count;
    }


}