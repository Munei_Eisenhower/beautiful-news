package com.mmbadimunei.beatifulnews.ui;

import static com.mmbadimunei.beatifulnews.ui.SplashScreenActivity.ARTICLE_KEY;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;

import com.keiferstone.nonet.NoNet;
import com.mmbadimunei.beatifulnews.Connection.CheckPermission;
import com.mmbadimunei.beatifulnews.Connection.MyReceiver;
import com.mmbadimunei.beatifulnews.R;
import com.mmbadimunei.beatifulnews.model.Article;

import io.reactivex.functions.Consumer;

public class MainActivity extends AppCompatActivity implements HeadlinesFragment.OnHeadlineSelectedListener {

    public static int ASSOCIATEDARTICLEURLTARGET = 2;
    private SharedPreferences preferences;
    private boolean canRemind;
    private MyReceiver myReceiver = new MyReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        canRemind = preferences.getBoolean(SettingsFragment.SETREMINDER, true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setSharedElementExitTransition(null);
        }
        CheckPermission.verifyStoragePermissions(this);

        NoNet.monitor(this)
                .poll()
                .snackbar()
                .observe()
                .subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer integer) throws Exception {
                        new HeadlinesFragment().monitorCallback.onConnectionEvent(integer);
                    }
                });

        if (canRemind) {
            myReceiver.setAlarm(this);

        } else {
            myReceiver.cancelAlarm(this);
        }

        HeadlinesFragment firstFragment = new HeadlinesFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.listFragment, firstFragment, "list").commit();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            getWindow().setAllowEnterTransitionOverlap(false);
        }

    }

    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {

            super.onBackPressed();
        } else {
            finish();

        }
    }

    @Override
    public Bundle getBundle() {
        return getIntent().getExtras();
    }

    @Override
    public void onArticleSelected(Object obj) {
        Article article = (Article) obj;

        //To handle SPONSORED article with no body article

        if (article.getAssociatedArticleUrlTarget() != ASSOCIATEDARTICLEURLTARGET) {

            displayPopup(article);

        } else {

            Intent intent = new Intent(MainActivity.this, ArticleDetailsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(ARTICLE_KEY, article);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    public void displayPopup(final Article article) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this,  android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(getResources().getString(R.string.sponser_aritcle_popup_title))
                .setMessage(getResources().getString(R.string.popup_msg_open_browser))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Uri uriUrl = Uri.parse(article.getArticleUrl());
                        Intent shareIntent = new Intent(Intent.ACTION_VIEW, uriUrl);
                        startActivity(shareIntent);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
