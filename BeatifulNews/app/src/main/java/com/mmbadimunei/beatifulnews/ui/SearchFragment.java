package com.mmbadimunei.beatifulnews.ui;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mmbadimunei.beatifulnews.R;
import com.mmbadimunei.beatifulnews.adapter.MyAdapterSearch;
import com.mmbadimunei.beatifulnews.api.ApiService;
import com.mmbadimunei.beatifulnews.api.RetroClient;
import com.mmbadimunei.beatifulnews.model.ArticleList;
import com.mmbadimunei.beatifulnews.model.ArticleSearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment implements SearchView.OnQueryTextListener, MyAdapterSearch.LoadMoreListener {

    private RecyclerView mRecyclerView;
    private MyAdapterSearch mAdapter;
    private String SITENAMEKEY = "siteName";
    private String SITENAME = "Netwerk24";
    private String PAGENUMBERKEY = "PageNumber";
    private int PAGENUMBER = 1;
    private String PAGESIZEKEY = "PageSize";
    private int PAGESIZE = 10;
    private String SEARCHTERMKEY = "SearchTerm";
    private String SEARCHTERM = "";
    private ImageView imageView;
    private SearchView searchView;
    private RecyclerView.LayoutManager mLayoutManager;
    private final long DELAY = 2000;
    private List<ArticleSearch> articleList = new ArrayList<>();
    private View mainView;
    private Handler mHandler = new Handler();

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_search, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imageView = (ImageView) getActivity().findViewById(R.id.top_logo);

        searchView = (SearchView) getActivity().findViewById(R.id.tot_search);
        imageView.setVisibility(View.GONE);
        searchView.setVisibility(View.VISIBLE);
        searchView.setIconified(false);

        setUpRecycler();

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                imageView.setVisibility(View.VISIBLE);
                searchView.setVisibility(View.GONE);

                if (getFragmentManager() != null) {
                    if (getFragmentManager().getBackStackEntryCount() > 0) {
                        getFragmentManager().popBackStack();
                    }
                }

                return false;
            }
        });

        searchView.setOnQueryTextListener(this);

        setHasOptionsMenu(true);

        return mainView;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (query.length() > 2) {

            searchWord(query, false);
            searchView.clearFocus();
            mRecyclerView.requestFocus();
        }
        return true;
    }

    @Override
    public boolean onQueryTextChange(final String newText) {
        if (newText.length() > 2) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    searchWord(newText, false);

                }
            }, DELAY);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            getActivity().onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void searchWord(String word, final boolean isLoadmore) {
        SEARCHTERM = word;
        ApiService api = RetroClient.getApiServiceForSearch();

        Map<String, String> body = new HashMap<String, String>();
        body.put(SITENAMEKEY, SITENAME);
        body.put(PAGENUMBERKEY, String.valueOf(PAGENUMBER));
        body.put(PAGESIZEKEY, String.valueOf(PAGESIZE));
        body.put(SEARCHTERMKEY, SEARCHTERM);

        Call<ArticleList> call = api.savePost(body);
        call.enqueue(new Callback<ArticleList>() {
            @Override
            public void onResponse(Call<ArticleList> call, Response<ArticleList> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().getArticlesSearch().size() < 1) {

                            setViewLayout(R.layout.no_results_fragment);

                        } else {


                            articleList = response.body().getArticlesSearch();
                            if (isLoadmore) {
                                mAdapter.loadMoreData(articleList);
                            } else {
                                setViewLayout(R.layout.fragment_search);
                                setUpRecycler();
                                mAdapter.addData(articleList);
                            }
                        }
                    } else {

                        setViewLayout(R.layout.no_results_fragment);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    setViewLayout(R.layout.no_results_fragment);
                }


            }

            @Override
            public void onFailure(Call<ArticleList> call, Throwable t) {

            }
        });
    }

    private void setViewLayout(int id) {

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mainView = inflater.inflate(id, null);
        ViewGroup rootView = (ViewGroup) getView();
        rootView.removeAllViews();
        rootView.addView(mainView);
    }


    public void setUpRecycler() {
        mRecyclerView = (RecyclerView) mainView.findViewById(R.id.recycler_view);
        mAdapter = new MyAdapterSearch(getContext());
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        if (!getResources().getBoolean(R.bool.portrait_only)) {
            mLayoutManager = new GridLayoutManager(getContext(), 3);

        }
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setLoadMoreListener(this);

    }

    @Override
    public void onLoadMore(int position) {

        PAGENUMBER = (position / 10) + 1;
        searchWord(SEARCHTERM, true);

    }
}