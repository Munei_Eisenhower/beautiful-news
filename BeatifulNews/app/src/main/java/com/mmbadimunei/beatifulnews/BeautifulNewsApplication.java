package com.mmbadimunei.beatifulnews;

import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.keiferstone.nonet.NoNet;

import io.fabric.sdk.android.Fabric;

/**
 * Created by eisenhower on 6/16/17.
 */

public class BeautifulNewsApplication   extends MultiDexApplication {

    public static int CURRENTCONNECTION;
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        MultiDex.install(this);
        NoNet.configure()
                .endpoint(getResources().getString(R.string.google_check_link))
                .timeout(5);



    }


}
