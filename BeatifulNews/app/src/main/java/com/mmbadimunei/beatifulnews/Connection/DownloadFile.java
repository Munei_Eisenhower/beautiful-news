package com.mmbadimunei.beatifulnews.Connection;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

/**
 * Created by eisenhower on 6/28/17.
 */

public class DownloadFile {

    public static int IMAGE_SAVED = 0;
    public static int VIDEO_SAVED = 1;
    public static int HIDDEN = 0;
    public static int UNHIDDEN = 1;
    public static String[] PARENTDIRECTORY = {Environment.getExternalStorageDirectory()
            + File.separator + ".savedArticles", Environment.getExternalStorageDirectory()
            + File.separator + "savedArticles" + File.separator};

    public static HashMap<Integer, String> downloadFile(int type, String fileURL, String fileName) {
        String path = "none";
        final int TIMEOUT_CONNECTION = 10000;
        final int TIMEOUT_SOCKET = 60000;
        HashMap<Integer, String> savedResults = new HashMap<>();

        try {

            File rootDir = new File(PARENTDIRECTORY[HIDDEN]);
            if (!rootDir.exists()) {
                rootDir.mkdir();
            }

            File rootFile = new File(rootDir.getAbsolutePath() + File.separator + fileName);

            if (!rootDir.exists()) {
                rootFile = new File(PARENTDIRECTORY[UNHIDDEN] + fileName);

            }

            URL url = new URL(fileURL);
            long startTime = System.currentTimeMillis();


            URLConnection ucon = url.openConnection();


            ucon.setReadTimeout(TIMEOUT_CONNECTION);
            ucon.setConnectTimeout(TIMEOUT_SOCKET);

            if (rootFile.exists()) {
                rootFile.delete();
            }

            InputStream is = ucon.getInputStream();
            BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
            FileOutputStream outStream = new FileOutputStream(rootFile);
            byte[] buff = new byte[5 * 1024];


            int len;
            while ((len = inStream.read(buff)) != -1) {
                outStream.write(buff, 0, len);
            }


            outStream.flush();
            outStream.close();
            inStream.close();
            path = rootFile.getAbsolutePath();
            Log.i("Test", "download completed in for" + path + "  "
                    + ((System.currentTimeMillis() - startTime) / 1000)
                    + " sec");
        } catch (Exception e) {

            e.printStackTrace();
        }
        savedResults.put(type, path);
        return savedResults;
    }

    public static void deleteFile(String url) {
        File file = new File(url);
        if (file.exists()) {
            file.delete();
        }
    }

    public static void deleteDirectory() {

        deleteFiles(PARENTDIRECTORY[HIDDEN]);
        deleteFiles(PARENTDIRECTORY[UNHIDDEN]);

    }

    public static void deleteFiles(String directory) {
        File dir = new File(directory);
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                new File(dir, children[i]).delete();
            }
        }
    }
}
