package com.mmbadimunei.beatifulnews.ui;

import static com.keiferstone.nonet.ConnectionStatus.CONNECTED;
import static com.mmbadimunei.beatifulnews.ui.SplashScreenActivity.ARTICLE_KEY;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.keiferstone.nonet.Monitor;
import com.keiferstone.nonet.NoNet;
import com.longtailvideo.jwplayer.JWPlayerView;
import com.longtailvideo.jwplayer.events.listeners.VideoPlayerEvents;
import com.mmbadimunei.beatifulnews.Connection.ArticleContentProvider;
import com.mmbadimunei.beatifulnews.Connection.BeautifulTable;
import com.mmbadimunei.beatifulnews.Connection.DownloadFile;
import com.mmbadimunei.beatifulnews.Connection.VideoDownloadLoader;
import com.mmbadimunei.beatifulnews.JWPlayer.KeepScreenOnHandler;
import com.mmbadimunei.beatifulnews.JWPlayer.PlayListItemSetUp;
import com.mmbadimunei.beatifulnews.R;
import com.mmbadimunei.beatifulnews.api.ApiService;
import com.mmbadimunei.beatifulnews.api.RetroClient;
import com.mmbadimunei.beatifulnews.model.Article;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleDetailsActivity extends AppCompatActivity implements VideoPlayerEvents.OnFullscreenListener, LoaderManager.LoaderCallbacks<HashMap<Integer, String>> {

    private JWPlayerView mPlayerView;
    private TextView mTextViewBody;
    private TextView mTextViewTitle;
    private TextView mTextViewDate;
    private TextView mTextViewViews;
    private Article mArticle;
    private boolean isSaved = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        }
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_article_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mPlayerView = (JWPlayerView) findViewById(R.id.jwplayer);
        mTextViewTitle = (TextView) findViewById(R.id.article_text_title);
        mTextViewBody = (TextView) findViewById(R.id.article_text_body);
        mTextViewDate = (TextView) findViewById(R.id.article_text_date);
        mTextViewViews = (TextView) findViewById(R.id.article_text_views);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mArticle = (Article) bundle.getSerializable(ARTICLE_KEY);
            if (mArticle != null) {
                isSaved = mArticle.isSaved();
            }
        }

        mTextViewTitle.setText(mArticle.getTitle());
        mTextViewDate.setText(mArticle.getDisplayDate());
        mTextViewViews.setText(mArticle.getArticleViews() +getResources(). getString(R.string.views));

        if (isSaved) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                mTextViewBody.setText(Html.fromHtml(mArticle.getArticleBody(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                mTextViewBody.setText(Html.fromHtml(mArticle.getArticleBody()));
            }

        }


        mPlayerView.addOnFullscreenListener(this);

        new FloatButtonAction(this, mArticle.getTitle(), mArticle.getArticleUrl());

        new KeepScreenOnHandler(mPlayerView, getWindow());


        NoNet.check(this)
                .toast().callback(new Monitor.Callback() {
            @Override
            public void onConnectionEvent(int connectionStatus) {
                if (connectionStatus == CONNECTED) {

                    if (!isSaved) {
                        ApiService api = RetroClient.getApiService();

                        Call<Article> call = api.getBODY(mArticle.getArticleId());

                        call.enqueue(new Callback<Article>() {
                            @Override
                            public void onResponse(Call<Article> call, Response<Article> response) {

                                if (response.isSuccessful()) {

                                    mArticle = response.body();

                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        mTextViewBody.setText(Html.fromHtml(mArticle.getArticleBody(), Html.FROM_HTML_MODE_COMPACT));
                                    } else {
                                        mTextViewBody.setText(Html.fromHtml(mArticle.getArticleBody()));
                                    }

                                    if (mArticle.getGalleryArticleItemModel() != null && mArticle.getGalleryArticleItemModel().getVideoUrl() != null) {

                                        try {
                                            if (mArticle.getGalleryArticleItemModel().getVideoUrl().isEmpty() || mArticle.getGalleryArticleItemModel().getVideoUrl().equalsIgnoreCase("none")) {
                                                setBackgroundImageToPlayer(mArticle.getMediumThumbnailUrl());
                                            } else {
                                                mPlayerView.load(
                                                        PlayListItemSetUp.getPlayList(mArticle.getGalleryArticleItemModel().getVideoUrl(), mArticle.getMediumThumbnailUrl(),
                                                                mArticle.getBlurb()));
                                            }
                                        } catch (Exception e) {

                                        }

                                    } else {
                                        Snackbar.make(mTextViewTitle, R.string.string_some_thing_wrong, Snackbar.LENGTH_LONG).show();

                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<Article> call, Throwable t) {

                            }
                        });
                    } else {

                        setupForLocal();

                    }

                } else {
                    setupForLocal();
                }

            }


        })
                .start();


        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        mPlayerView.setFullscreen(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE, true);
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onResume() {

        super.onResume();
        mPlayerView.onResume();
    }

    @Override
    protected void onPause() {

        mPlayerView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {

        mPlayerView.onDestroy();
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mPlayerView.getFullscreen()) {
                mPlayerView.setFullscreen(false, true);
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onFullscreen(boolean fullscreen) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (fullscreen) {
                actionBar.hide();
            } else {
                actionBar.show();
            }
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (isSaved) {
            MenuItem menuItem = menu.findItem(R.id.action_save);
            menuItem.setTitle(getResources().getString(R.string.delete_article_menu_item));
            invalidateOptionsMenu();
        }

        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            super.onBackPressed();

        } else if (id == R.id.action_save) {

            if (!isSaved) {
                getSupportLoaderManager().initLoader(1, null, this).forceLoad();

            } else {
                Uri uri = Uri.parse(ArticleContentProvider.CONTENT_URI_ARTICLES + "/"
                        + ArticleContentProvider.BASE_PATH);
                if (getContentResolver().delete(uri, BeautifulTable.ARTICLE_ID + " = ?", new String[]{mArticle.getArticleId()}) > 0) {

                    DownloadFile.deleteFile(mArticle.getImageLocalPath());
                    DownloadFile.deleteFile(mArticle.getVideoLocalPath());

                    Toast.makeText(this, getResources().getString(R.string.article_deleted), Toast.LENGTH_SHORT).show();
                    super.onBackPressed();
                } else {
                    Toast.makeText(this, getResources().getString(R.string.article_not_deleted_msg), Toast.LENGTH_SHORT).show();
                }


            }

        } else if (id == R.id.action_enter_comp) {

            Uri uriUrl = Uri.parse(mArticle.getArticleUrl());
            Intent shareIntent = new Intent(Intent.ACTION_VIEW, uriUrl);
            startActivity(shareIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<HashMap<Integer, String>> onCreateLoader(int id, Bundle args) {
        return new VideoDownloadLoader(ArticleDetailsActivity.this, mArticle.getArticleId(), mArticle.getGalleryArticleItemModel().getVideoUrl(), mArticle.getMediumThumbnailUrl());
    }

    @Override
    public void onLoadFinished(Loader<HashMap<Integer, String>> loader, HashMap<Integer, String> data) {

        Article articleToSave = mArticle;
        articleToSave.setSaved(true);

        articleToSave.setVideoLocalPath(data.get(DownloadFile.VIDEO_SAVED));
        articleToSave.setImageLocalPath(data.get(DownloadFile.IMAGE_SAVED));
        articleToSave.setAssociatedArticleUrlTarget(MainActivity.ASSOCIATEDARTICLEURLTARGET);
        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        articleToSave.setSavedDate(dateFormat.format(calendar.getTime()));

        try {
            getContentResolver().insert(ArticleContentProvider.CONTENT_URI_ARTICLES, BeautifulTable.getContentValue(articleToSave));
            Toast.makeText(this,getResources().getString( R.string.article_was_saved), Toast.LENGTH_SHORT).show();

        } catch (Exception r) {
            Toast.makeText(this,getResources().getString( R.string.article_was_not_saved), Toast.LENGTH_SHORT).show();
            r.printStackTrace();
        }
    }

    @Override
    public void onLoaderReset(Loader<HashMap<Integer, String>> loader) {
        Toast.makeText(this,getResources().getString( R.string.article_was_not_saved), Toast.LENGTH_SHORT).show();
    }

    public void setupForLocal() {
        try {
            if (mArticle.getVideoLocalPath() != null) {
                if (mArticle.getVideoLocalPath().isEmpty() || mArticle.getVideoLocalPath().equalsIgnoreCase("none")) {
                    //Create an imageView and set image to it so i can display something in case there is no video.
                    setBackgroundImageToPlayer(mArticle.getImageLocalPath());

                } else {
                    mPlayerView.load(PlayListItemSetUp.getPlayList(getResources().getString(R.string.local_file) + mArticle.getVideoLocalPath(),getResources().getString(R.string.local_file) + mArticle.getImageLocalPath(), mArticle.getBlurb()));
                }
            }
        } catch (Exception e) {

        }
    }

    public void setBackgroundImageToPlayer(String image) {

        final ImageView imageView = new ImageView(getApplicationContext());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mPlayerView.getWidth(), mPlayerView.getHeight());
        imageView.setLayoutParams(params);

        Picasso.with(this)
                .load(new File(image))
                .error(R.drawable.img_not_available)
                .placeholder(R.drawable.loading)
                .into(imageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        Drawable drawable = imageView.getDrawable();
                        mPlayerView.setBackground(drawable);
                    }

                    @Override
                    public void onError() {
                        Drawable drawable = imageView.getDrawable();
                        mPlayerView.setBackground(drawable);
                    }
                });

    }
}
