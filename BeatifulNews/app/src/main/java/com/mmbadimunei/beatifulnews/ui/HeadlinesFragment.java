package com.mmbadimunei.beatifulnews.ui;


import static com.keiferstone.nonet.ConnectionStatus.CONNECTED;
import static com.mmbadimunei.beatifulnews.Util.md5;
import static com.mmbadimunei.beatifulnews.ui.SplashScreenActivity.ADSLIST_KEY;
import static com.mmbadimunei.beatifulnews.ui.SplashScreenActivity.ARTICLE_KEY;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.NativeExpressAdView;
import com.keiferstone.nonet.ConnectionStatus;
import com.keiferstone.nonet.Monitor;
import com.mmbadimunei.beatifulnews.BeautifulNewsApplication;
import com.mmbadimunei.beatifulnews.BuildConfig;
import com.mmbadimunei.beatifulnews.Connection.ArticleContentProvider;
import com.mmbadimunei.beatifulnews.Connection.BeautifulTable;
import com.mmbadimunei.beatifulnews.R;
import com.mmbadimunei.beatifulnews.adapter.RecyclerViewAdapter;
import com.mmbadimunei.beatifulnews.api.ApiService;
import com.mmbadimunei.beatifulnews.api.RetroClient;
import com.mmbadimunei.beatifulnews.model.Article;
import com.mmbadimunei.beatifulnews.model.ArticleList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HeadlinesFragment extends Fragment implements RecyclerViewAdapter.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private List<Object> articleListWithAds = new ArrayList<>();
    private OnHeadlineSelectedListener mCallback;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private List<String> itemsAds = null;
    private String adsList = "";
    private ArrayList<Article> articleList = new ArrayList<>();

    @Override
    public void onItemClick(Object item) {
        mCallback.onArticleSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = ArticleContentProvider.CONTENT_URI_ARTICLES;
        return new CursorLoader(getContext(), uri, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        editor = preferences.edit();
        editor.putInt(BeautifulTable.ARTICLE_COUNT_PREF, data.getCount());
        editor.commit();

        ArrayList<Article> articles = new ArrayList<>();

        if (articleList.size() > 0) {

            for (Article article : articleList) {

                for (Article articleFromDB : BeautifulTable.getAllArticles(data)) {

                    if (article.getArticleId().equals(articleFromDB.getArticleId())) {

                        article = articleFromDB;
                    }
                }
                articles.add(article);
            }

        } else {
            articles = BeautifulTable.getAllArticles(data);
        }
        readData(articles);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public interface OnHeadlineSelectedListener {

        public Bundle getBundle();

        public void onArticleSelected(Object obj);
    }

    public HeadlinesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_headlines, container, false);

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
        toolbar.setLayoutParams(params);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);


        if (mCallback.getBundle() != null)  {
            try {

                if (mCallback.getBundle().containsKey(ARTICLE_KEY)) {

                    Bundle bundle = mCallback.getBundle();

                    articleList = (ArrayList<Article>) bundle.getSerializable(ARTICLE_KEY);

                }
                if(mCallback.getBundle().containsKey(ADSLIST_KEY)) {
                    adsList = mCallback.getBundle().getString(ADSLIST_KEY);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        itemsAds = Arrays.asList(adsList.split("\\s*,\\s*"));



        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);

        setUpRecylceView();

        setHasOptionsMenu(true);



        getActivity().getSupportLoaderManager().initLoader(0, null, HeadlinesFragment.this);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (BeautifulNewsApplication.CURRENTCONNECTION == CONNECTED) {
                    refreshItems();
                }
            }
        });

        return view;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.action_search) {

            SearchFragment searchFragment = new SearchFragment();
            final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.listFragment, searchFragment, getResources().getString(R.string.fragment_tag_search));
            transaction.addToBackStack(null);
            transaction.commit();

            return true;
        } else if (id == R.id.action_share_story) {
            ShareStoryFragment shareFragment = new ShareStoryFragment();
            final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.listFragment, shareFragment, getResources().getString(R.string.fragment_tag_share));
            transaction.addToBackStack(null);
            transaction.commit();

        } else if (id == R.id.action_settings) {
            SettingsFragment settingsFragment = new SettingsFragment();
            final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.listFragment, settingsFragment,getResources(). getString(R.string.fragment_tag_settings));
            transaction.addToBackStack(null);
            transaction.commit();
        } else if (id == R.id.action_about) {

            Uri uriUrl = Uri.parse(getResources().getString(R.string.about_link));
            Intent shareIntent = new Intent(Intent.ACTION_VIEW, uriUrl);
            startActivity(shareIntent);

        }else if(id==R.id.action_offer){

            Uri uriUrl = Uri.parse(getResources().getString(R.string.offer_link));
            Intent shareIntent = new Intent(Intent.ACTION_VIEW, uriUrl);
            startActivity(shareIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        hideSearch();
    }

    public void hideSearch() {
        SearchView searchView = (SearchView) getActivity().findViewById(R.id.tot_search);
        if (searchView != null) {
            searchView.setIconified(true);
            searchView.setVisibility(View.GONE);
            ImageView imageView = (ImageView) getActivity().findViewById(R.id.top_logo);
            imageView.setVisibility(View.VISIBLE);

        }
    }

    void refreshItems() {

        if (BeautifulNewsApplication.CURRENTCONNECTION == CONNECTED) {

            ApiService api = RetroClient.getApiService();

            Call<ArticleList> call = api.getMyJSON();

            call.enqueue(new Callback<ArticleList>() {
                @Override
                public void onResponse(Call<ArticleList> call, Response<ArticleList> response) {

                    if (response.isSuccessful()) {

                        articleList = response.body().getArticles();


                        readData(articleList);
                        getActivity().getSupportLoaderManager().initLoader(0, null, HeadlinesFragment.this).forceLoad();

                    } else {
                        Snackbar.make(getView(), R.string.string_some_thing_wrong, Snackbar.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<ArticleList> call, Throwable t) {

                }
            });


        } else {
            readData(articleList);
            getActivity().getSupportLoaderManager().initLoader(0, null, HeadlinesFragment.this).forceLoad();
        }


        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }


    public void readData(ArrayList<Article> articleList) {
        if (articleList != null) {


            try {

                articleListWithAds.clear();
                articleListWithAds.addAll(articleList);
                if (BeautifulNewsApplication.CURRENTCONNECTION == CONNECTED) {
                    try {
                        if(getResources().getBoolean(R.bool.portrait_only))
                        addNativeExpressAds();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                adapter.adAds(articleListWithAds);
                adapter.setOnEventListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void addNativeExpressAds() {


        View adviews = getActivity().getLayoutInflater().inflate(R.layout.native_express_ad_container, null);
        final NativeExpressAdView adView = (NativeExpressAdView) adviews.findViewById(R.id.adViewS);
        if (itemsAds.size() > 1) {
            for (int i = 0; i < itemsAds.size(); i++) {
                if (!articleListWithAds.isEmpty()) {
                    articleListWithAds.add(Integer.parseInt(itemsAds.get(i)), adView);
                }
            }

            setUpAndLoadNativeExpressAds(adView);
        }
    }

    private void setUpAndLoadNativeExpressAds(final NativeExpressAdView mAdView) {

        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {

                if (itemsAds.size() > 1) {

                    mAdView.setAdListener(new AdListener() {
                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();

                        }

                        @Override
                        public void onAdFailedToLoad(int errorCode) {

                            Log.e("MainActivity", "The previous Native Express ad failed to load. Attempting to"
                                    + " load the next Native Express ad in the items list.");

                        }
                    });

                    String deviceId = "";
                    if(BuildConfig.DEBUG) {
                        try {

                            String android_id = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
                            deviceId = md5(android_id).toUpperCase();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    AdRequest adRequest = new AdRequest.Builder()
                            .addTestDevice(deviceId)
                            .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                            .build();

                    mAdView.loadAd(adRequest);
                    if (adapter != null) {
                        mRecyclerView.smoothScrollToPosition(0);
                    }


                }
            }
        });
    }

    public void setUpRecylceView() {
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        if(!getResources().getBoolean(R.bool.portrait_only)){
           mLayoutManager = new GridLayoutManager(getContext(), 3);
         }
        if (BeautifulNewsApplication.CURRENTCONNECTION == CONNECTED) {
            try {
                addNativeExpressAds();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        adapter = new RecyclerViewAdapter(getActivity());
        mRecyclerView.setAdapter(adapter);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallback = (OnHeadlineSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mRecyclerView != null && mSwipeRefreshLayout != null && adapter != null) {
            getActivity().getSupportLoaderManager().initLoader(0, null, HeadlinesFragment.this).forceLoad();

        }

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();


    }

    public Monitor.Callback monitorCallback = new Monitor.Callback() {
        @Override
        public void onConnectionEvent(@ConnectionStatus int connectionStatus) {

            BeautifulNewsApplication.CURRENTCONNECTION = connectionStatus;
            if (mRecyclerView != null && mSwipeRefreshLayout != null && articleList.size() < 1) {
                refreshItems();
            }
        }
    };
}
