package com.mmbadimunei.beatifulnews.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Article implements Serializable {


    @SerializedName("articleId")
    @Expose
    String articleId;
    @SerializedName("categoryName")
    @Expose
    String categoryName;
    @SerializedName("associatedArticleUrlTarget")
    @Expose
    int associatedArticleUrlTarget;
    @SerializedName("categoryBreadcrumb")
    @Expose
    String categoryBreadcrumb;
    @SerializedName("categoryUrl")
    @Expose
    String categoryUrl;
    @SerializedName("displayDate")
    @Expose
    String displayDate;
    @SerializedName("thumbnailUrl")
    @Expose
    String ThumbnailUrl;
    @SerializedName("largeThumbnailUrl")
    @Expose
    String largeThumbnailUrl;
    @SerializedName("mediumThumbnailUrl")
    @Expose
    String mediumThumbnailUrl;
    @SerializedName("smallThumbnailUrl")
    @Expose
    String smallThumbnailUrl;
    @SerializedName("blurb")
    @Expose
    String blurb;
    @SerializedName("title")
    @Expose
    String title;
    @SerializedName("articleUrl")
    @Expose
    String articleUrl;
    @SerializedName("articleBody")
    @Expose
    String articleBody;
    @SerializedName("articleViews")
    @Expose
    long articleViews;
    @SerializedName("showArticleViews")
    @Expose
    boolean showArticleViews;
    @SerializedName("includeShareOptions")
    @Expose
    boolean includeShareOptions;
    @SerializedName("isPremium")
    @Expose
    boolean isPremium;
    @SerializedName("galleryArticleItemModel")
    @Expose
    GalleryArticleItemModel GalleryArticleItemModel;

    public String getLargeThumbnailUrl() {
        return largeThumbnailUrl;
    }

    public void setLargeThumbnailUrl(String largeThumbnailUrl) {
        this.largeThumbnailUrl = largeThumbnailUrl;
    }

    public String getMediumThumbnailUrl() {
        return mediumThumbnailUrl;
    }

    public void setMediumThumbnailUrl(String mediumThumbnailUrl) {
        this.mediumThumbnailUrl = mediumThumbnailUrl;
    }

    public String getSmallThumbnailUrl() {
        return smallThumbnailUrl;
    }

    public void setSmallThumbnailUrl(String smallThumbnailUrl) {
        this.smallThumbnailUrl = smallThumbnailUrl;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryBreadcrumb() {
        return categoryBreadcrumb;
    }

    public void setCategoryBreadcrumb(String categoryBreadcrumb) {
        this.categoryBreadcrumb = categoryBreadcrumb;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    public String getDisplayDate() {
        if (displayDate.contains("T")) {

            String dtStart = displayDate;
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
            try {
                Date date = format.parse(dtStart);
                SimpleDateFormat sdf = new SimpleDateFormat("d MMMM yyyy");
                displayDate = sdf.format(date);

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }


        return displayDate;
    }

    public void setDisplayDate(String displayDate) {
        this.displayDate = displayDate;
    }

    public String getThumbnailUrl() {
        return ThumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        ThumbnailUrl = thumbnailUrl;
    }

    public String getBlurb() {
        return blurb;
    }

    public void setBlurb(String blurb) {
        this.blurb = blurb;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArticleUrl() {
        return articleUrl;
    }

    public void setArticleUrl(String articleUrl) {
        this.articleUrl = articleUrl;
    }

    public String getArticleBody() {
        return articleBody;
    }

    public void setArticleBody(String articleBody) {
        this.articleBody = articleBody;
    }

    public long getArticleViews() {


        return articleViews;
    }

    public int getAssociatedArticleUrlTarget() {
        return associatedArticleUrlTarget;
    }

    public void setAssociatedArticleUrlTarget(int associatedArticleUrlTarget) {
        this.associatedArticleUrlTarget = associatedArticleUrlTarget;
    }

    public void setArticleViews(long articleViews) {
        this.articleViews = articleViews;
    }

    public boolean isShowArticleViews() {
        return showArticleViews;
    }

    public void setShowArticleViews(boolean showArticleViews) {
        this.showArticleViews = showArticleViews;
    }

    public boolean isIncludeShareOptions() {
        return includeShareOptions;
    }

    public void setIncludeShareOptions(boolean includeShareOptions) {
        this.includeShareOptions = includeShareOptions;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public void setPremium(boolean premium) {
        isPremium = premium;
    }

    public GalleryArticleItemModel getGalleryArticleItemModel() {
        return GalleryArticleItemModel;
    }

    public void setGalleryArticleItemModel(GalleryArticleItemModel galleryArticleItemModel) {
        GalleryArticleItemModel = galleryArticleItemModel;
    }

    public class GalleryArticleItemModel {
        @SerializedName("url")
        @Expose
        private String videoUrl;

        public String getVideoUrl() {

            return videoUrl.replace("[[resolution]]", "480");
        }

        public void setVideoUrl(String videoUrl) {
            this.videoUrl = videoUrl;
        }
    }

    private boolean isSaved = false;
    private String savedDate = "";
    private String videoLocalPath = "";
    private String imageLocalPath = "";

    public String getSavedDate() {
        return savedDate;
    }

    public void setSavedDate(String savedDate) {
        this.savedDate = savedDate;
    }

    public String getVideoLocalPath() {
        return videoLocalPath;
    }

    public void setVideoLocalPath(String videoLocalPath) {
        this.videoLocalPath = videoLocalPath;
    }

    public String getImageLocalPath() {
        return imageLocalPath;
    }

    public void setImageLocalPath(String imageLocalPath) {
        this.imageLocalPath = imageLocalPath;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean saved) {
        isSaved = saved;
    }
}