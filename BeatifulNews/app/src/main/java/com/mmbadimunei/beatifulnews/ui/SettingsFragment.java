package com.mmbadimunei.beatifulnews.ui;


import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.mmbadimunei.beatifulnews.Connection.ArticleContentProvider;
import com.mmbadimunei.beatifulnews.Connection.BeautifulTable;
import com.mmbadimunei.beatifulnews.Connection.DatabaseHelper;
import com.mmbadimunei.beatifulnews.Connection.DownloadFile;
import com.mmbadimunei.beatifulnews.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {
    private TextView textViewNumber;
    private Button textViewContactMe;
    private Button buttonClear;
    private DatabaseHelper databaseHelper;
    private int countArticles = 0;
    private Switch aSwitchSave;
    private Switch aSwitchReminder;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    public static String SAVEVIDEOOFFLINE = "saveVideo";
    public static String SETREMINDER = "canRemind";
    private boolean saveVideo;
    private boolean canRemind;

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        databaseHelper = new DatabaseHelper(getActivity());

        countArticles = databaseHelper.getArticleCount();
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        editor = preferences.edit();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        textViewNumber = (TextView) view.findViewById(R.id.textViewArticleNumber);
        textViewContactMe = (Button) view.findViewById(R.id.textViewContactMe);

        aSwitchSave = (Switch) view.findViewById(R.id.switchSave);
        aSwitchReminder = (Switch) view.findViewById(R.id.switch_reminder);
        buttonClear = (Button) view.findViewById(R.id.buttonClear);


        saveVideo = preferences.getBoolean(SAVEVIDEOOFFLINE, false);
        canRemind = preferences.getBoolean(SETREMINDER, true);
        countArticles = preferences.getInt(BeautifulTable.ARTICLE_COUNT_PREF, 0);

        textViewNumber.setText(countArticles + "");
        aSwitchSave.setChecked(saveVideo);
        aSwitchReminder.setChecked(canRemind);

        aSwitchReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                editor.putBoolean(SETREMINDER, isChecked);
                editor.commit();
            }
        });

        aSwitchSave.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                editor.putBoolean(SAVEVIDEOOFFLINE, isChecked);
                editor.commit();
            }
        });

        textViewContactMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uriUrl = Uri.parse(getResources().getString(R.string.contact_me_link));
                Intent shareIntent = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(shareIntent);
            }
        });

        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDatabase();
            }
        });

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            getActivity().onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    public void deleteDatabase() {

        Uri uri = ArticleContentProvider.CONTENT_URI_ARTICLES;

        try {
            if (getActivity().getContentResolver().delete(uri, null, null) > 0) {

                DownloadFile.deleteDirectory();

                Toast.makeText(getActivity(), getResources().getString(R.string.article_cleared), Toast.LENGTH_SHORT).show();

                editor.putInt(BeautifulTable.ARTICLE_COUNT_PREF, 0);
                editor.commit();

                textViewNumber.setText(0 + "");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), getResources().getString(R.string.article_not_cleared), Toast.LENGTH_SHORT).show();
        }

    }

}
