package com.mmbadimunei.beatifulnews.JWPlayer;

import android.support.annotation.NonNull;

import com.longtailvideo.jwplayer.media.playlists.PlaylistItem;

/**
 * Created by eisenhower on 6/29/17.
 */

public class PlayListItemSetUp {
public static PlaylistItem getPlayList(@NonNull String file,@NonNull String image,@NonNull String description) {

    PlaylistItem playlistItem = new PlaylistItem.Builder()
            .file(file)
            .image(image)
            .description(description)
            .build();
   return playlistItem;
}
}
