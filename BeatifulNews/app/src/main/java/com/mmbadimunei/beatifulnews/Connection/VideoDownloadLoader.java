package com.mmbadimunei.beatifulnews.Connection;

import static com.mmbadimunei.beatifulnews.Connection.DownloadFile.downloadFile;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.AsyncTaskLoader;

import com.mmbadimunei.beatifulnews.ui.SettingsFragment;

import java.util.HashMap;

/**
 * Created by eisenhower on 6/28/17.
 */

public class VideoDownloadLoader extends AsyncTaskLoader<HashMap<Integer, String>> {
    private String articleId;
    private String articleVideoUrl;
    private String articleImage;
    private SharedPreferences preferences;
    private boolean saveVideo = false;

    public VideoDownloadLoader(Context context, String articleId, String articleVideoUrl, String articleImage) {
        super(context);
        this.articleId = articleId;
        this.articleVideoUrl = articleVideoUrl;
        this.articleImage = articleImage;

        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        saveVideo = preferences.getBoolean(SettingsFragment.SAVEVIDEOOFFLINE, false);
    }


    @Override
    public HashMap<Integer, String> loadInBackground() {
        String pathVideo = "none";
        String pathImage;
        HashMap<Integer, String> savedResults = new HashMap<>();

        pathImage = downloadFile(DownloadFile.IMAGE_SAVED, articleImage, articleId + ".jpg").get(DownloadFile.IMAGE_SAVED);

        if (saveVideo) {
            if (!articleVideoUrl.equals("none") || articleVideoUrl != null) {

                pathVideo = downloadFile(DownloadFile.VIDEO_SAVED, articleVideoUrl, articleId + ".mp4").get(DownloadFile.VIDEO_SAVED);
            }
        }
        savedResults.put(DownloadFile.IMAGE_SAVED, pathImage);
        savedResults.put(DownloadFile.VIDEO_SAVED, pathVideo);

        return savedResults;
    }

}
