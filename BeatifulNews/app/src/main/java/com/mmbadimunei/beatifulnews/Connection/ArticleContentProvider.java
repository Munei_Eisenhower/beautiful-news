package com.mmbadimunei.beatifulnews.Connection;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

/**
 * Created by eisenhower on 6/27/17.
 */

public class ArticleContentProvider extends ContentProvider {


    public static final String PROVIDER_NAME = "beautiful.news.article.";

    public static final String BASE_PATH = "article";
    private static final String BASE_PATH_ALL = "articles";

    public static final Uri CONTENT_URI_ARTICLES = Uri.parse("content://" + PROVIDER_NAME + "/" + BASE_PATH_ALL);


    private static final int ARTICLE_ID = 10;
    private static final int ARTICLES = 20;

    private static final UriMatcher uriMatcher = new UriMatcher(
            UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(PROVIDER_NAME, BASE_PATH_ALL, ARTICLES);
        uriMatcher.addURI(PROVIDER_NAME, BASE_PATH_ALL + "/" + BASE_PATH, ARTICLE_ID);
    }


    private DatabaseHelper mDatabaseHelper;

    @Override
    public boolean onCreate() {
        mDatabaseHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        if (uriMatcher.match(uri) == ARTICLES) {

            return mDatabaseHelper.getAllArticles();
        } else {
            return null;
        }
    }


    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SQLiteDatabase sqlDB = mDatabaseHelper.getWritableDatabase();
        int uriType = uriMatcher.match(uri);
        long id = 0;
        if (uriType == ARTICLES) {
            id = sqlDB.insert(BeautifulTable.TABLE_ARTICLE, null, values);
        }
        getContext().getContentResolver().notifyChange(uri, null);


        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int uriType = uriMatcher.match(uri);
        SQLiteDatabase sqlDB = mDatabaseHelper.getWritableDatabase();

        int rowsDeleted = 0;
        switch (uriType) {
            case ARTICLES:
                rowsDeleted = sqlDB.delete(BeautifulTable.TABLE_ARTICLE, "1", null);
                break;
            case ARTICLE_ID:
                String id = selectionArgs[0];

                if (!TextUtils.isEmpty(id)) {
                    rowsDeleted = sqlDB.delete(
                            BeautifulTable.TABLE_ARTICLE,
                            BeautifulTable.ARTICLE_ID + "=" + "'" + id + "'",
                            null);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
