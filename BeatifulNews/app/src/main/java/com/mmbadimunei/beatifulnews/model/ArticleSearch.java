package com.mmbadimunei.beatifulnews.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by eisenhower on 5/10/17.
 */

public class ArticleSearch {


    @SerializedName("articleId")
    @Expose
    String articleId;

    @SerializedName("publishedDate")
    @Expose
    String publishedDate;

    @SerializedName("displayDate")
    @Expose
    String displayDate;

    @SerializedName("thumbnailLargeURL")
    @Expose
    String thumbnailLargeURL;

    @SerializedName("thumbnailMediumURL")
    @Expose
    String thumbnailMediumURL;

    @SerializedName("thumbnailURL")
    @Expose
    String thumbnailURL;

    //@SerializedName("images")
   // @Expose
   // Images images;

    @SerializedName("author")
    @Expose
    String author;

    @SerializedName("title")
    @Expose
    String title;

    @SerializedName("mobileURL")
    @Expose
    String mobileURL;

    @SerializedName("articleURL")
    @Expose
    String articleURL;

    @SerializedName("body")
    @Expose
    String body;

    @SerializedName("totalEntities")
    @Expose
    long totalEntities;

    @SerializedName("pageSize")
    @Expose
  long pageSize;

    @SerializedName("videoURL")
    @Expose
    String videoURL;

    @SerializedName("pageNo")
    @Expose
    long pageNo;



    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getDisplayDate() {
        return displayDate;
    }

    public void setDisplayDate(String displayDate) {
        this.displayDate = displayDate;
    }

    public String getThumbnailLargeURL() {
        return thumbnailLargeURL;
    }

    public void setThumbnailLargeURL(String thumbnailLargeURL) {
        this.thumbnailLargeURL = thumbnailLargeURL;
    }

    public String getThumbnailMediumURL() {
        return thumbnailMediumURL;
    }

    public void setThumbnailMediumURL(String thumbnailMediumURL) {
        this.thumbnailMediumURL = thumbnailMediumURL;
    }

    public String getThumbnailURL() {
        return thumbnailURL;
    }

    public void setThumbnailURL(String thumbnailURL) {
        this.thumbnailURL = thumbnailURL;
    }

   // public Images getImages() {
    //    return images;
    //}

   // public void setImages(Images images) {
    //    this.images = images;
   // }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMobileURL() {
        return mobileURL;
    }

    public void setMobileURL(String mobileURL) {
        this.mobileURL = mobileURL;
    }

    public String getArticleURL() {
        return articleURL;
    }

    public void setArticleURL(String articleURL) {
        this.articleURL = articleURL;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getTotalEntities() {
        return totalEntities;
    }

    public void setTotalEntities(long totalEntities) {
        this.totalEntities = totalEntities;
    }

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public long getPageNo() {
        return pageNo;
    }

    public void setPageNo(long pageNo) {
        this.pageNo = pageNo;
    }

  /*  public class Images  {
        @SerializedName("url")
        @Expose
        private String image;

        @SerializedName("Caption")
        @Expose
        private String Caption;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCaption() {
            return Caption;
        }

        public void setCaption(String caption) {
            Caption = caption;
        }
    }*/
}
