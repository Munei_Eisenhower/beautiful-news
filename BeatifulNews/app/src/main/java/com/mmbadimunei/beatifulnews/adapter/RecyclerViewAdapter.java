package com.mmbadimunei.beatifulnews.adapter;

import static com.keiferstone.nonet.ConnectionStatus.CONNECTED;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.NativeExpressAdView;
import com.mmbadimunei.beatifulnews.BeautifulNewsApplication;
import com.mmbadimunei.beatifulnews.R;
import com.mmbadimunei.beatifulnews.model.Article;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@link RecyclerViewAdapter} class.
 * <p>The adapter provides access to the items in the {@link MenuItemViewHolder}
 * or the {@link NativeExpressAdViewHolder}.</p>
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int MENU_ITEM_VIEW_TYPE = 0;


    private static final int NATIVE_EXPRESS_AD_VIEW_TYPE = 1;



    private Context context;
    // The list of Native Express ads and menu items.
    private List<Object> mRecyclerViewItems = new ArrayList<>();

    private OnItemClickListener listener;


    public RecyclerViewAdapter(Context context) {

        this.context = context;



    }

    public void addAds(List<Object> objectList) {
        mRecyclerViewItems = objectList;
        notifyDataSetChanged();
    }
    public void setOnEventListener(OnItemClickListener listener) {
        this.listener = listener;
    }
    public interface OnItemClickListener {
        void onItemClick(Object item);
    }

    /**
     * The {@link MenuItemViewHolder} class.
     * Provides a reference to each view in the menu item view.
     */
    public class MenuItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title, views, date, textViewOffline;
        public ImageView coverPicture;

        MenuItemViewHolder(View view) {
            super(view);
            title = (TextView) itemView.findViewById(R.id.my_text_title);
            views = (TextView) itemView.findViewById(R.id.article_text_views);
            date = (TextView) itemView.findViewById(R.id.article_text_date);
            textViewOffline = (TextView) itemView.findViewById(R.id.isSavedOffline);
            coverPicture = (ImageView) itemView.findViewById(R.id.cover_picture);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (!mRecyclerViewItems.isEmpty()) {


                if(listener!=null) {
                    if( BeautifulNewsApplication.CURRENTCONNECTION==CONNECTED ||((Article)mRecyclerViewItems.get(getAdapterPosition())).isSaved() )
                    listener.onItemClick(mRecyclerViewItems.get(getAdapterPosition()));
                }

            }

        }
    }

    /**
     * The {@link NativeExpressAdViewHolder} class.
     */
    public class NativeExpressAdViewHolder extends RecyclerView.ViewHolder {

        NativeExpressAdViewHolder(View view) {
            super(view);
        }
    }

    @Override
    public int getItemCount() {
        return mRecyclerViewItems.size();
    }

    /**
     * Determines the view type for the given position.
     */
    @Override
    public int getItemViewType(int position) {

        if (mRecyclerViewItems.get(position) instanceof Article) {
            return MENU_ITEM_VIEW_TYPE;
        } else {
            return NATIVE_EXPRESS_AD_VIEW_TYPE;
        }


    }

    public void adAds(List<Object> objectList) {
        mRecyclerViewItems = objectList;
        notifyDataSetChanged();

    }

    /**
     * Creates a new view for a menu item view or a Native Express ad view
     * based on the viewType. This method is invoked by the layout manager.
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case MENU_ITEM_VIEW_TYPE:
                View menuItemLayoutView = LayoutInflater.from(viewGroup.getContext()).inflate(
                        R.layout.list_item_card, viewGroup, false);
                return new MenuItemViewHolder(menuItemLayoutView);
            case NATIVE_EXPRESS_AD_VIEW_TYPE:
                // fall through
            default:
                View nativeExpressLayoutView = LayoutInflater.from(
                        viewGroup.getContext()).inflate(R.layout.native_express_ad_container,
                        viewGroup, false);
                return new NativeExpressAdViewHolder(nativeExpressLayoutView);
        }

    }

    /**
     * Replaces the content in the views that make up the menu item view and the
     * Native Express ad view. This method is invoked by the layout manager.
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case MENU_ITEM_VIEW_TYPE:
              final  MenuItemViewHolder menuItemHolder = (MenuItemViewHolder) holder;
              final  Article article = (Article) mRecyclerViewItems.get(position);

                // Get the menu item image resource ID.
                if (article.getArticleViews() == 0) {
                    menuItemHolder.views.setVisibility(View.INVISIBLE);

                } else {
                    menuItemHolder.views.setVisibility(View.VISIBLE);
                }

                if(article.isSaved()){
                    menuItemHolder.textViewOffline.setVisibility(View.VISIBLE);
                }else{
                    menuItemHolder.textViewOffline.setVisibility(View.INVISIBLE);
                }


                menuItemHolder.title.setText(article.getTitle());

                menuItemHolder.views.setText(article.getArticleViews() + " views");
                menuItemHolder.date.setText(article.getDisplayDate());

                    Picasso.with(context)
                            .load(article.getMediumThumbnailUrl())
                            .networkPolicy(NetworkPolicy.OFFLINE)
                            .error(R.drawable.img_not_available)
                            .placeholder(R.drawable.loading)
                            .into(menuItemHolder.coverPicture, new Callback() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onError() {
                                    //Try again online if cache failed
                                    Picasso.with(context)
                                            .load(article.getMediumThumbnailUrl())
                                            .error(R.drawable.img_not_available)
                                            .placeholder(R.drawable.loading)
                                            .into(menuItemHolder.coverPicture, new Callback() {
                                                @Override
                                                public void onSuccess() {

                                                }

                                                @Override
                                                public void onError() {
                                                    Log.v("Picasso","Could not fetch image");
                                                }
                                            });
                                }
                            });

               // } else {

                    //imageLoader.displayImage(article.getSmallThumbnailUrl(),menuItemHolder.coverPicture, options, null);
               //     Picasso.with(context).load(article.getThumbnailUrl()).placeholder(R.drawable.loading).error(R.drawable.img_not_available).into(menuItemHolder.coverPicture);


              //  }
                break;
            case NATIVE_EXPRESS_AD_VIEW_TYPE:
                // fall through
            default:
                NativeExpressAdViewHolder nativeExpressHolder =
                        (NativeExpressAdViewHolder) holder;
                NativeExpressAdView adView =
                        (NativeExpressAdView) mRecyclerViewItems.get(position);

                ViewGroup adCardView = (ViewGroup) nativeExpressHolder.itemView;
                // The NativeExpressAdViewHolder recycled by the RecyclerView may be a different
                // instance than the one used previously for this position. Clear the
                // NativeExpressAdViewHolder of any subviews in case it has a different
                // AdView associated with it, and make sure the AdView for this position doesn't
                // already have a parent of a different recycled NativeExpressAdViewHolder.
                if (adCardView.getChildCount() > 0) {
                    adCardView.removeAllViews();
                }
                if (adView.getParent() != null) {
                    ((ViewGroup) adView.getParent()).removeView(adView);
                }

                // Add the Native Express ad to the native express ad view.
                adCardView.addView(adView);
        }
    }


}


