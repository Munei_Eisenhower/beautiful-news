package com.mmbadimunei.beatifulnews.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mmbadimunei.beatifulnews.R;
import com.mmbadimunei.beatifulnews.model.ArticleSearch;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eisenhower on 5/5/17.
 */

public class MyAdapterSearch extends RecyclerView.Adapter<MyAdapterSearch.MyViewHolder> {

    private List<ArticleSearch> articles = new ArrayList<>();
    private Context context;

    private LoadMoreListener listener;

    public void setLoadMoreListener(LoadMoreListener listener) {
        this.listener = listener;
    }

    public interface LoadMoreListener {
        void onLoadMore(int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title, views, date, textViewOffline;
        public ImageView coverPicture;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.my_text_title);
            views = (TextView) itemView.findViewById(R.id.article_text_views);
            date = (TextView) itemView.findViewById(R.id.article_text_date);

            textViewOffline = (TextView) itemView.findViewById(R.id.isSavedOffline);
            coverPicture = (ImageView) itemView.findViewById(R.id.cover_picture);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }


    public MyAdapterSearch(Context context) {
        this.context = context;

    }

    public void addData(List<ArticleSearch> articleList) {

        articles = articleList;
        notifyDataSetChanged();

    }

    public void loadMoreData(List<ArticleSearch> articleList) {

        articles.addAll(articles.size() - 1, articleList);
        notifyDataSetChanged();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        ArticleSearch article = articles.get(position);


        holder.title.setText(article.getTitle());

        holder.date.setText(article.getDisplayDate());

        String thumbnail = "none";

        thumbnail = (article.getThumbnailMediumURL() != null) ? article.getThumbnailMediumURL() : (article.getThumbnailLargeURL() != null) ? article.getThumbnailLargeURL() : thumbnail;

        Picasso.with(context).load(thumbnail).placeholder(R.drawable.loading).error(R.drawable.img_not_available).into(holder.coverPicture);


        if ((position >= getItemCount() - 1)) {
            if (listener != null) {

                listener.onLoadMore(getItemCount());
            }
        }
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }


    public List<ArticleSearch> getData() {
        return articles;
    }

}