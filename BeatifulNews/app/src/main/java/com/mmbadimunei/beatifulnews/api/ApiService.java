package com.mmbadimunei.beatifulnews.api;


import com.mmbadimunei.beatifulnews.model.Article;
import com.mmbadimunei.beatifulnews.model.ArticleList;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;


public interface ApiService {



    @GET("list?cb=Beautiful-News&rankOption=3&sortOrder=1&includeArticleViews=true&pageSize=9999999")
    Call<ArticleList> getMyJSON();

    @GET
    Call<Article> getBODY(@Url String url);

    @Headers("Content-Type: application/json")
    @POST("Articles.svc/search?keywords=1/")

    Call<ArticleList> savePost(@Body Map<String,String> body);
}
