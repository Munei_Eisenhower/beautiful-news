package com.mmbadimunei.beatifulnews.Connection;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mmbadimunei.beatifulnews.model.Article;

import java.util.ArrayList;

/**
 * Created by eisenhower on 6/27/17.
 */

public class BeautifulTable {
    public static final String TABLE_ARTICLE = "beautifulNewsArticles";

    public static final String COL_ID = "col_id";
    public static final String ARTICLE_ID = "articleId";
    public static final String ARTICLE_URL = "articleUrl";
    public static final String ARTICLE_TITLE = "title";
    public static final String ARTICLE_SMALL_THUMBNAIL = "smallThumbnailUrl";
    public static final String ARTICLE_MEDIUM_THUMBNAIL = "mediumThumbnailUrl";
    public static final String ARTICLE_LARGE_THUMBNAIL = "largeThumbnailUrl";
    public static final String ARTICLE_DATE = "displayDate";
    public static final String ARTICLE_VIEWS = "articleViews";
    public static final String ARTICLE_VIDEO_URL = "articleVideoUrl";
    public static final String ARTICLE_VIDEO_FILE_PATH = "videoLocalPath";
    public static final String ARTICLE_IMAGE_FILE_PATH = "imageLocalPath";
    public static final String ARTICLE_CATEGORYNAME = "categoryName";
    public static final String ARTICLE_ASSOCIATEDARTICLEURLTARGET = "associatedArticleUrlTarget";
    public static final String ARTICLE_BODY = "articleBody";
    public static final String ARTICLE_DATE_SAVED = "dateSaved";
    public static final String ARTICLE_IS_SAVED = "isSaved";
    public static final String ARTICLE_COUNT_PREF = "article.count";


    public static void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_ARTICLE_TABLE = "CREATE TABLE " + TABLE_ARTICLE + "("
                + COL_ID + " INTEGER PRIMARY KEY ,"
                + ARTICLE_ID + " TEXT unique,"
                + ARTICLE_TITLE + " TEXT ,"
                + ARTICLE_URL + " TEXT ,"
                + ARTICLE_BODY + " TEXT ,"
                + ARTICLE_SMALL_THUMBNAIL + " TEXT ,"
                + ARTICLE_MEDIUM_THUMBNAIL + " TEXT ,"
                + ARTICLE_LARGE_THUMBNAIL + " TEXT ,"
                + ARTICLE_DATE + " TEXT ,"
                + ARTICLE_CATEGORYNAME + " TEXT ,"
                + ARTICLE_VIEWS + " INTEGER ,"
                + ARTICLE_ASSOCIATEDARTICLEURLTARGET + " INTEGER ,"
                + ARTICLE_VIDEO_URL + " TEXT ,"
                + ARTICLE_VIDEO_FILE_PATH + " TEXT ,"
                + ARTICLE_IMAGE_FILE_PATH + " TEXT ,"
                + ARTICLE_DATE_SAVED + " TEXT ,"
                + ARTICLE_IS_SAVED + " INTEGER )";
        sqLiteDatabase.execSQL(CREATE_ARTICLE_TABLE);
    }


    public static void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTICLE);
        onCreate(sqLiteDatabase);
    }

    public static ArrayList<Article> getAllArticles(Cursor cursor) {
        ArrayList<Article> articleList = new ArrayList<>();
        if (cursor != null) {

            Article article;
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {

                        article = new Article();
                        try {
                            article.setArticleId(cursor.getString(cursor.getColumnIndex(ARTICLE_ID)));
                            article.setTitle(cursor.getString(cursor.getColumnIndex(ARTICLE_TITLE)));
                            article.setArticleUrl(cursor.getString(cursor.getColumnIndex(ARTICLE_URL)));
                            article.setArticleBody(cursor.getString(cursor.getColumnIndex(ARTICLE_BODY)));
                            article.setSmallThumbnailUrl(cursor.getString(cursor.getColumnIndex(ARTICLE_SMALL_THUMBNAIL)));
                            article.setMediumThumbnailUrl(cursor.getString(cursor.getColumnIndex(ARTICLE_MEDIUM_THUMBNAIL)));
                            article.setLargeThumbnailUrl(cursor.getString(cursor.getColumnIndex(ARTICLE_LARGE_THUMBNAIL)));
                            article.setDisplayDate(cursor.getString(cursor.getColumnIndex(ARTICLE_DATE)));
                            article.setArticleViews(cursor.getLong(cursor.getColumnIndex(ARTICLE_VIEWS)));
                            article.setAssociatedArticleUrlTarget(cursor.getInt(cursor.getColumnIndex(ARTICLE_ASSOCIATEDARTICLEURLTARGET)));
                            article.setCategoryName(cursor.getString(cursor.getColumnIndex(ARTICLE_CATEGORYNAME)));
                            article.setVideoLocalPath(cursor.getString(cursor.getColumnIndex(ARTICLE_VIDEO_FILE_PATH)));
                            article.setImageLocalPath(cursor.getString(cursor.getColumnIndex(ARTICLE_IMAGE_FILE_PATH)));
                            article.setSavedDate(cursor.getString(cursor.getColumnIndex(ARTICLE_DATE_SAVED)));
                            Boolean isSaved = (cursor.getInt(cursor.getColumnIndexOrThrow(ARTICLE_IS_SAVED)) == 1) ? true : false;
                            article.setSaved(isSaved);
                            articleList.add(article);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } while (cursor.moveToNext());
                }
            }
        }

        return articleList;
    }

    public static ContentValues getContentValue(Article article) {
        ContentValues values = new ContentValues();
        values.put(ARTICLE_ID, article.getArticleId());
        values.put(ARTICLE_URL, article.getArticleUrl());
        values.put(ARTICLE_TITLE, article.getTitle());
        values.put(ARTICLE_BODY, article.getArticleBody());
        values.put(ARTICLE_DATE, article.getDisplayDate());
        values.put(ARTICLE_VIEWS, article.getArticleViews());
        values.put(ARTICLE_VIDEO_FILE_PATH, article.getVideoLocalPath());
        values.put(ARTICLE_IMAGE_FILE_PATH, article.getImageLocalPath());
        values.put(ARTICLE_ASSOCIATEDARTICLEURLTARGET, article.getAssociatedArticleUrlTarget());
        values.put(ARTICLE_CATEGORYNAME, article.getCategoryName());
        values.put(ARTICLE_VIDEO_URL, article.getGalleryArticleItemModel().getVideoUrl());
        values.put(ARTICLE_SMALL_THUMBNAIL, article.getSmallThumbnailUrl());
        values.put(ARTICLE_MEDIUM_THUMBNAIL, article.getMediumThumbnailUrl());
        values.put(ARTICLE_LARGE_THUMBNAIL, article.getLargeThumbnailUrl());
        values.put(ARTICLE_DATE_SAVED, article.getSavedDate());
        int isSaved = (article.isSaved()) ? 1 : 0;
        values.put(ARTICLE_IS_SAVED, isSaved);

        return values;
    }


}
