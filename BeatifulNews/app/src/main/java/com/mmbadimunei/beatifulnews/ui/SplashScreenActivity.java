package com.mmbadimunei.beatifulnews.ui;

import static com.keiferstone.nonet.ConnectionStatus.CONNECTED;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.keiferstone.nonet.Monitor;
import com.keiferstone.nonet.NoNet;
import com.mmbadimunei.beatifulnews.BeautifulNewsApplication;
import com.mmbadimunei.beatifulnews.BuildConfig;
import com.mmbadimunei.beatifulnews.R;
import com.mmbadimunei.beatifulnews.api.ApiService;
import com.mmbadimunei.beatifulnews.api.RetroClient;
import com.mmbadimunei.beatifulnews.model.ArticleList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SplashScreenActivity extends AppCompatActivity {
    private ProgressBar mprogressBar;
    private View parentView;
    private ImageView imageView_logo;
    private ImageView imageView_logo_benz;
    private ObjectAnimator anim;
    private String myConfig;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
public static String ARTICLE_KEY = "article";
public static String ADSLIST_KEY = "adsList";
    private int progress_start = 0;
    private int progress_end = 100;
    private int cache_time = 3600;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mprogressBar = (ProgressBar) findViewById(R.id.circular_progress_bar);
        parentView = findViewById(R.id.root);
        imageView_logo = (ImageView) findViewById(R.id.logo);
        imageView_logo_benz = (ImageView) findViewById(R.id.logo_benz);

        StartAnimations();
        addProgressBar();

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);

        fetchConfig();

        NoNet.check(this)
                .toast().callback(new Monitor.Callback() {
            @Override
            public void onConnectionEvent(int connectionStatus) {

                BeautifulNewsApplication.CURRENTCONNECTION = connectionStatus;

                if (connectionStatus == CONNECTED) {

                    ApiService api = RetroClient.getApiService();

                    Call<ArticleList> call = api.getMyJSON();

                    call.enqueue(new Callback<ArticleList>() {
                        @Override
                        public void onResponse(Call<ArticleList> call, Response<ArticleList> response) {

                            if (response.isSuccessful()) {

                                if (anim != null) {
                                    anim.cancel();
                                }
                                Bundle bundle = new Bundle();
                                bundle.putSerializable(ARTICLE_KEY, response.body().getArticles());
                                startMain(bundle);

                            } else {
                                Snackbar.make(parentView, R.string.string_some_thing_wrong, Snackbar.LENGTH_LONG).show();

                                if (anim != null) {
                                    anim.cancel();
                                }

                                startMain(null);
                            }
                        }

                        @Override
                        public void onFailure(Call<ArticleList> call, Throwable t) {

                            if (anim != null) {
                                anim.cancel();
                            }

                            startMain(null);
                        }
                    });

                } else {
                    Snackbar.make(parentView, R.string.string_internet_connection_not_available, Snackbar.LENGTH_INDEFINITE).show();
                    if (anim != null) {
                        anim.cancel();
                    }

                    mprogressBar.setVisibility(View.GONE);

                    startMain(null);
                }
            }


        })
                .start();


    }

    public void addProgressBar() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                anim = ObjectAnimator.ofInt(mprogressBar, getResources().getString(R.string.progress), progress_start, progress_end);
                anim.setDuration(3000);
                anim.setInterpolator(new DecelerateInterpolator());
                anim.setRepeatCount(ValueAnimator.INFINITE);
                anim.setRepeatMode(ValueAnimator.RESTART);
                anim.start();

            }
        }).run();

    }

    private void startMain(Bundle bundle) {
        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.putExtra(ADSLIST_KEY, myConfig);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Pair[] pairs = new Pair[2];
            pairs[0] = new Pair<View, String>(imageView_logo,getResources(). getString(R.string.brand_logo_shared_id));
            pairs[1] = new Pair<View, String>(imageView_logo_benz, getResources().getString(R.string.sponsor_logo_shared_id));
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, pairs);
            try {
                startActivity(intent, options.toBundle());
            } catch (Exception r) {
                startActivity(intent);
                r.printStackTrace();

            }
            overridePendingTransition(0, 0);
        } else {

            startActivity(intent);
        }
    }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(SplashScreenActivity.this, R.anim.alpha);
        anim.reset();
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.root);

        relativeLayout.clearAnimation();
        relativeLayout.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();

        imageView_logo.clearAnimation();
        imageView_logo.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate2);
        anim.reset();

        imageView_logo_benz.clearAnimation();
        imageView_logo_benz.startAnimation(anim);


    }

    private void fetchConfig() {

        long cacheExpiration = cache_time;

        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            mFirebaseRemoteConfig.activateFetched();
                        }

                        myConfig = mFirebaseRemoteConfig.getValue(getString(R.string.config_ads_key)).asString();

                    }
                });

    }
}
